package com.eduoglobal.eduomess;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.eduoglobal.eduomess.Model.Mess;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class MessDetails extends AppCompatActivity {

//	private ProgressBar spinner;
	ImageView profileImageView;
	TextView nameTextView, regNumberTextView, messStatusTextView;

	private Mess mess;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_outpass_details);
		Toolbar toolbar = findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		profileImageView = findViewById(R.id.profile_image);
		nameTextView = findViewById(R.id.name);
		regNumberTextView = findViewById(R.id.regNumber);
		messStatusTextView = findViewById(R.id.messStatus);

		SharedPreferences userDetails = getSharedPreferences("UserProfile", MODE_PRIVATE);
		final String userToken = userDetails.getString("token", "");

		FloatingActionButton fab = findViewById(R.id.fab);
		fab.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent scannerIntent = new Intent(MessDetails.this, MainActivity.class);
				startActivity(scannerIntent);
				//kill em
				finish();
			}
		});

		Intent intent = getIntent();
		String messToken = intent.getStringExtra("JWT");

		fetchOutpass(messToken, userToken);
	}

	private void fetchOutpass(String messToken, final String userToken) {
		try {
			RequestQueue requestQueue = Volley.newRequestQueue(this);
			String URL = "https://api.iar.ankuranant.dev/mess";
			JSONObject jsonBody = new JSONObject();
			jsonBody.put("userToken", messToken);
			final String mRequestBody = jsonBody.toString();

			StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
				@Override
				public void onResponse(String response) {
					try {
						//server response
						Log.i("LOG_VOLLEY", response);
						JSONObject responseJson = new JSONObject(response);

						mess = new Mess(
								responseJson.getString("name"),
								responseJson.getString("regNumber"),
								responseJson.getString("avatarUrl"),
								responseJson.getString("message")
						);
						handleUI(mess);

					} catch(JSONException jsonEx) {
						jsonEx.printStackTrace();
					}
				}
			}, new Response.ErrorListener() {
				@Override
				public void onErrorResponse(VolleyError error) {
					Log.e("LOG_VOLLEY", error.toString());
					Toast.makeText(getApplicationContext(), "Invalid token", Toast.LENGTH_LONG).show();
					nameTextView.setText("Invalid Token");
					nameTextView.setTextColor(Color.RED);
				}
			}) {
				@Override
				public String getBodyContentType() {
					return "application/json; charset=utf-8";
				}

				@Override
				public byte[] getBody() throws AuthFailureError {
					try {
						return mRequestBody == null ? null : mRequestBody.getBytes("utf-8");
					} catch (UnsupportedEncodingException uee) {
						VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", mRequestBody, "utf-8");
						return null;
					}
				}

				@Override
				public Map<String, String> getHeaders() throws AuthFailureError {
					Map<String, String> headers = new HashMap<String, String>();
					headers.put("Authorization", "Bearer " + userToken);
					return headers;
				}

				@Override
				protected Response<String> parseNetworkResponse(NetworkResponse response) {
					String responseString = "";
					if (response != null) {

						responseString = String.valueOf(response.statusCode);

					}
					return super.parseNetworkResponse(response);
				}
			};

			requestQueue.add(stringRequest);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private void handleUI(Mess mess) {
		Toast.makeText(this, "test22", Toast.LENGTH_SHORT).show();
		Glide.with(MessDetails.this)
				.load(mess.getAvatarUrl())
				.placeholder(R.drawable.user_avatar_holder)
				.into(profileImageView);

		nameTextView.setText(mess.getName());
		regNumberTextView.setText(mess.getRegNumber());
		messStatusTextView.setText(mess.getMessage());

	}

}
