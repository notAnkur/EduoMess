package com.eduoglobal.eduomess.Model;

public class Mess {
	private String name, regNumber, avatarUrl, message;

	public Mess(String name, String regNumber, String avatarUrl, String message) {
		this.name = name;
		this.regNumber = regNumber;
		this.avatarUrl = avatarUrl;
		this.message = message;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRegNumber() {
		return regNumber;
	}

	public void setRegNumber(String regNumber) {
		this.regNumber = regNumber;
	}

	public String getAvatarUrl() {
		return avatarUrl;
	}

	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
